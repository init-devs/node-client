const tracer = require('tracer')

module.exports = tracer.colorConsole({
  format: '[{{file}}:{{line}}:{{pos}}] <{{title}}>: {{message}}',
})

